#ifndef MODIFIER_DRONE_H
#define MODIFIER_DRONE_H

#include <QDialog>
#include "ui_mainwindow.h"
#include "Drone.h"

namespace Ui {
class modifier_drone ;
}

class modifier_drone : public QDialog
{
    Q_OBJECT

public:
    explicit modifier_drone(QWidget *parent = nullptr);
    ~modifier_drone();




    QString getID2();

    void setID2(QString value);
private slots:
    void on_buttonBox_rejected();

    void on_buttonBox_accepted();

    void recupID(QString);

    void on_pushButton_clicked();

private:
    Ui::modifier_drone *ui;

    QString ID2;


};

#endif // MODIFIER_DRONE_H
