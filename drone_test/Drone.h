#ifndef DRONE_H
#define DRONE_H
#include <QString>
#include <iostream>
using namespace std;


class Drone
{
    public:
        Drone(unsigned int =0, string="", double=0, double=0, double=0, double=0, double=0);
        virtual ~Drone();

        string Gettype() { return type; }
        void Settype(string val) { type = val; }
        double Getvitesse() { return vitesse; }
        void Setvitesse(double val) { vitesse = val; }

        double GetOEW() { return OEW; }
        void SetOEW(double val) { OEW = val; }
        double GetMTOW() { return MTOW; }
        void SetMTOW(double val) { MTOW = val; }
        double Getautonomie() { return autonomie; }
        void Setautonomie(double val) { autonomie = val; }
        double Getconsomation() { return consomation; }
        void Setconsomation(double val) { consomation = val; }
        unsigned int Getnumero() { return numero; }
        void Setnumero(unsigned int val) { numero = val; }

        void affiche();

        void recupDrone(string);
        void saveDrone();

        void setCharge_utile_max();

        double getCharge_utile_max() const;

protected:

private:
        string type;
        double vitesse;
        double charge_utile_max;
        double OEW;
        double MTOW;
        double autonomie;
        double consomation;
        unsigned int numero;
};

#endif // DRONE_H
