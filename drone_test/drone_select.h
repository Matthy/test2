#ifndef DRONE_SELECT_H
#define DRONE_SELECT_H

#include <QDialog>
#include "Drone.h"

namespace Ui {
class drone_select;
}

class drone_select : public QDialog
{
    Q_OBJECT

public:
    explicit drone_select(QWidget *parent = nullptr);
    ~drone_select();

private slots:
    void on_buttonBox_rejected();

    void on_buttonBox_accepted();

private:
    Ui::drone_select *ui;
    Drone *drn;
};

#endif // DRONE_SELECT_H
