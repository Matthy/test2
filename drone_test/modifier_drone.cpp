#include "modifier_drone.h"
#include "ui_modifier_drone.h"
#include <QDebug>
#include <QString>

modifier_drone::modifier_drone(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::modifier_drone)
{
    ui->setupUi(this);


    Drone *drn = new Drone();
    qDebug()<<"drone crée";



    drn->recupDrone(getID2().toStdString()+".txt");

    ui->ln_type->setText(QString::fromStdString(drn->Gettype()));
    ui->ln_num->setText(QString::number((drn->Getnumero())));
    ui->ln_vitesse->setText(QString::number(drn->Getvitesse()));
    ui->ln_OEW->setText(QString::number(drn->GetOEW()));
    ui->ln_MTOW->setText(QString::number(drn->GetMTOW()));
    ui->ln_autonomie->setText(QString::number(drn->Getautonomie()));
    ui->ln_conso->setText(QString::number(drn->Getconsomation()));





}

modifier_drone::~modifier_drone()
{
    delete ui;
}


void modifier_drone::on_buttonBox_rejected()
{
    this->close();
}

void modifier_drone::on_buttonBox_accepted()
{
    Drone *drn = new Drone();

    drn->Setnumero(ui->ln_num->text().toUInt());
    drn->Settype(ui->ln_type->text().toStdString());
    drn->Setvitesse(ui->ln_vitesse->text().toDouble());
    drn->SetOEW(ui->ln_OEW->text().toDouble());
    drn->SetMTOW(ui->ln_MTOW->text().toDouble());
    drn->Setautonomie(ui->ln_autonomie->text().toDouble());
    drn->Setconsomation(ui->ln_conso->text().toDouble());
    drn->setCharge_utile_max();

    drn->saveDrone();
}

void modifier_drone::recupID(QString value)
{
    setID2(value);
}

QString modifier_drone::getID2()
{
    return ID2;
}

void modifier_drone::setID2(QString value)
{
    ui->ln_num->setText(value);

    Drone *drn = new Drone();
    qDebug()<<"drone crée";




    drn->recupDrone(ui->ln_num->text().toStdString()+".txt");

    ui->ln_type->setText(QString::fromStdString(drn->Gettype()));
    //ui->ln_num->setText(QString::number((drn->Getnumero())));
    ui->ln_vitesse->setText(QString::number(drn->Getvitesse()));
    ui->ln_OEW->setText(QString::number(drn->GetOEW()));
    ui->ln_MTOW->setText(QString::number(drn->GetMTOW()));
    ui->ln_autonomie->setText(QString::number(drn->Getautonomie()));
    ui->ln_conso->setText(QString::number(drn->Getconsomation()));
}

void modifier_drone::on_pushButton_clicked()
{

}
