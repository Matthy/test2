#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Drone.h"
using namespace std;
Drone::Drone(unsigned int n, string t, double v, double o, double m, double a, double c)
{
    Setnumero(n);
    Settype(t);
    Setvitesse(v);
    SetOEW(o);
    SetMTOW(m);
    Setautonomie(a);
    Setconsomation(c);
    setCharge_utile_max();

}

void Drone::saveDrone()
{

    string fname="D:/Travails/AJC Formation/PROJET/Drone/drone_test/Drones/"+to_string(Getnumero())+".txt";
    cout<<fname<<endl;
    string const fdrone(fname) ;
    ofstream fd(fdrone.c_str());

    if(fd)
    {
        fd<<Getnumero()<<endl;
        fd<<Gettype()<<endl;
        fd<<Getvitesse()<<endl;        
        fd<<GetOEW()<<endl;
        fd<<GetMTOW()<<endl;
        fd<<Getautonomie()<<endl;
        fd<<Getconsomation()<<endl;
        fd<<getCharge_utile_max()<<endl;

        cout<<"Drone : "<<Getnumero()<<" enregistre."<<endl;

    }
    else
    {
        cout<<"Erreur fichier. (verifiez le Dossier Drones)"<<endl;
    }
    fd.close();
}

void Drone::setCharge_utile_max()
{
    charge_utile_max = GetMTOW()-GetOEW();
}

double Drone::getCharge_utile_max() const
{
    return charge_utile_max;
}

void Drone::recupDrone(string path)
{

    string fname="D:/Travails/AJC Formation/PROJET/Drone/drone_test/Drones/"+path;
    ifstream fd(fname);

    if(fd)
    {


        string data;


        getline(fd,data);
        Setnumero(stoul(data));

        getline(fd,data);
        Settype(data);

        getline(fd,data);
        Setvitesse(stod(data));



        getline(fd,data);
        SetOEW(stod(data));

        getline(fd,data);
        SetMTOW(stod(data));

        getline(fd,data);
        Setautonomie(stod(data));

        getline(fd,data);
        Setconsomation(stod(data));

        getline(fd,data);
        setCharge_utile_max();


    }
    else
    {
        cout<<"Erreur fichier. (verifiez le Dossier Drones)"<<endl;
    }

    fd.close();

}

void Drone::affiche()
{

    cout<<endl<<"Drone ID : "<<Getnumero()<<endl;
    cout<<"vitesse : "<<Getvitesse()<<"km/h"<<endl;
    cout<<"Charge utile Max: "<<getCharge_utile_max()<<"kg"<<endl;
    cout<<"OEW : "<<GetOEW()<<"kg"<<endl;
    cout<<"MTOW : "<<GetMTOW()<<"kg"<<endl;
    cout<<"Autonomie : "<<Getautonomie()<<"H"<<endl;
    cout<<"Consomation : "<<Getconsomation()<<"L/h"<<endl;
}


Drone::~Drone()
{
    //cout<<"destruction du drone : "<<Getnumero();
}
