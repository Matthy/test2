#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Drone.h"
#include <qdebug.h>
#include <QDirIterator>
#include <QStandardItemModel>
#include <QString>
#include <qstring.h>
#include "drone_select.h"
#include <QMessageBox>
#include "modifier_drone.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->tableWidget,SIGNAL(cellClicked(int, int)),this,SLOT(setID()));





   // this->setWindowTitle("Mega test list drones !!!");

    QStringList tle;
    ui->tableWidget->setColumnCount(3); // set le nbr de colones
    tle<<"ID"<<"Type"<<"Autonomie";
    ui->tableWidget->setHorizontalHeaderLabels(tle); //edit de l'entete


    // recherche des fichier drones dans le dossier

    QDirIterator drone_f("D:/Travails/AJC Formation/PROJET/Drone/drone_test/Drones/",QStringList()<<"*.txt");

    while (drone_f.hasNext())
    {
        drone_f.next();
        qDebug()<<drone_f.filePath();
        qDebug()<<"1";

        ui->tableWidget->insertRow(ui->tableWidget->rowCount());

        qDebug()<<"2";

        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,0,new QTableWidgetItem(drone_f.fileName().remove(".txt")));

        qDebug()<<"3";

        Drone *d = new Drone();
        d->recupDrone(drone_f.fileName().toStdString());

        qDebug()<<"4";


        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,1,new QTableWidgetItem(QString::fromStdString(d->Gettype())));
        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,2,new QTableWidgetItem(QString::number(d->Getautonomie())));
    }

    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui->pushButton_2->hide();

    ui->pushButton->setEnabled(false);






    //connect(ui->btn_modif,SIGNAL(on_btn_modif_clicked()),Ui::modifier_drone)



}

MainWindow::~MainWindow()
{
    delete ui;
}



QString MainWindow::getID()
{
    return ID;
}

void MainWindow::setID()
{
    ID=ui->tableWidget->item(ui->tableWidget->currentRow(),0)->text();
    qDebug()<<"set ID"+ui->tableWidget->item(ui->tableWidget->currentRow(),0)->text();

}

void MainWindow::on_pushButton_clicked()
{
    qDebug()<<ui->tableWidget->item(ui->tableWidget->currentRow(),0)->text();
    Drone *d=new Drone();
    d->recupDrone((ui->tableWidget->item(ui->tableWidget->currentRow(),0)->text().toStdString()+".txt"));
    qDebug()<<QString::fromStdString(d->Gettype());
    ui->label->setText("Drone selectionne de type : "+QString::fromStdString(d->Gettype()));


}

void MainWindow::on_action_Drones_triggered()
{
    drone_select *ajtdrnbox = new drone_select();
    ajtdrnbox->exec();


    ui->tableWidget->setRowCount(0);


    QDirIterator drone_f("D:/Travails/AJC Formation/PROJET/Drone/drone_test/Drones/",QStringList()<<"*.txt");

    while (drone_f.hasNext())
    {
        drone_f.next();
        qDebug()<<drone_f.filePath();
        qDebug()<<"1";

        ui->tableWidget->insertRow(ui->tableWidget->rowCount());

        qDebug()<<"2";

        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,0,new QTableWidgetItem(drone_f.fileName().remove(".txt")));

        qDebug()<<"3";

        Drone *d = new Drone();
        d->recupDrone(drone_f.fileName().toStdString());

        qDebug()<<"4";


        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,1,new QTableWidgetItem(QString::fromStdString(d->Gettype())));
        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,2,new QTableWidgetItem(QString::number(d->Getautonomie())));
    }
}

void MainWindow::on_btn_filtre_clicked()
{
    ui->pushButton_2->show();
    QString filter = ui->ln_search->text();
    for( int i = 0; i < ui->tableWidget->rowCount(); ++i )
    {
        bool match = false;
        for( int j = 0; j < ui->tableWidget->columnCount(); ++j )
        {
            QTableWidgetItem *item = ui->tableWidget->item( i, j );
            if( item->text().contains(filter) )
            {
                match = true;
                break;
            }
        }
        ui->tableWidget->setRowHidden( i, !match );
    }
}

void MainWindow::on_btn_modif_clicked()
{



    modifier_drone *mdfdrnbox = new modifier_drone();
    mdfdrnbox->setID2(getID());
    mdfdrnbox->exec();


}



void MainWindow::on_pushButton_2_clicked()
{

    ui->tableWidget->setRowCount(0);

    QDirIterator drone_f("D:/Travails/AJC Formation/PROJET/Drone/drone_test/Drones/",QStringList()<<"*.txt");

    while (drone_f.hasNext())
    {
        drone_f.next();
        qDebug()<<drone_f.filePath();
        qDebug()<<"1";

        ui->tableWidget->insertRow(ui->tableWidget->rowCount());

        qDebug()<<"2";

        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,0,new QTableWidgetItem(drone_f.fileName().remove(".txt")));

        qDebug()<<"3";

        Drone *d = new Drone();
        d->recupDrone(drone_f.fileName().toStdString());

        qDebug()<<"4";


        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,1,new QTableWidgetItem(QString::fromStdString(d->Gettype())));
        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,2,new QTableWidgetItem(QString::number(d->Getautonomie())));
    }

    ui->pushButton_2->hide();

}

void MainWindow::on_btn_supp_clicked()
{

    QFile f("D:/Travails/AJC Formation/PROJET/Drone/drone_test/Drones/"+ui->tableWidget->item(ui->tableWidget->currentRow(),0)->text()+".txt");
    f.remove();

    ui->tableWidget->setRowCount(0);

    QDirIterator drone_f("D:/Travails/AJC Formation/PROJET/Drone/drone_test/Drones/",QStringList()<<"*.txt");

    while (drone_f.hasNext())
    {
        drone_f.next();
        qDebug()<<drone_f.filePath();
        qDebug()<<"1";

        ui->tableWidget->insertRow(ui->tableWidget->rowCount());

        qDebug()<<"2";

        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,0,new QTableWidgetItem(drone_f.fileName().remove(".txt")));

        qDebug()<<"3";

        Drone *d = new Drone();
        d->recupDrone(drone_f.fileName().toStdString());

        qDebug()<<"4";


        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,1,new QTableWidgetItem(QString::fromStdString(d->Gettype())));
        ui->tableWidget->setItem(ui->tableWidget->rowCount() -1,2,new QTableWidgetItem(QString::number(d->Getautonomie())));
    }

}

void MainWindow::on_tableWidget_cellClicked(int row, int column)
{
    //activer le bouton quand on clique sur une celule pour la premiere fois
    ui->pushButton->setEnabled(true);
}

void MainWindow::on_action_Modification_rapide_triggered()
{
    QMessageBox *msgbx = new QMessageBox;
    msgbx->setWindowTitle("Modification rapide");
    msgbx->setText("Attention : cette fonction permet la modifications des données drone directement dans le tableau, sans confirmation !!!");
    msgbx->setIcon(QMessageBox::Warning);
    msgbx->setStandardButtons(QMessageBox::Yes|QMessageBox::No);
            int res = msgbx->exec();

    if (res==QMessageBox::Yes)
    {
        ui->tableWidget->setEditTriggers(QAbstractItemView::EditTriggers(2));
    }


}

void MainWindow::on_tableWidget_itemChanged(QTableWidgetItem *item)
{

}

