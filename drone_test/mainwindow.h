#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "Drone.h"
#include "modifier_drone.h"

#include <QMainWindow>
#include <QTableWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void envoiID(double);


    QString getID() ;


private slots:
    void on_pushButton_clicked();

    void on_action_Drones_triggered();

    void on_btn_filtre_clicked();

    void on_btn_modif_clicked();

void setID();

    void on_pushButton_2_clicked();

    void on_btn_supp_clicked();

    void on_tableWidget_cellClicked(int row, int column);

    void on_action_Modification_rapide_triggered();

    void on_tableWidget_itemChanged(QTableWidgetItem *item);

signals :

    void SenvoiID(double);



private:
    Ui::MainWindow *ui;
    Drone *drn = nullptr;

    QString ID;

};

#endif // MAINWINDOW_H
