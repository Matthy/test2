#include "drone_select.h"
#include "ui_drone_select.h"
using namespace std;
#include <iostream>
#include <string>
#include "ui_mainwindow.h"


drone_select::drone_select(QWidget *parent) :
    ui(new Ui::drone_select)
{
    ui->setupUi(this);
}

drone_select::~drone_select()
{
    delete ui;
}

void drone_select::on_buttonBox_rejected()
{
    this->close();
}

void drone_select::on_buttonBox_accepted()
{
    Drone *drn = new Drone();

    drn->Setnumero(ui->ln_num->text().toUInt());
    drn->Settype(ui->ln_type->text().toStdString());
    drn->Setvitesse(ui->ln_vitesse->text().toDouble());
    drn->SetOEW(ui->ln_OEW->text().toDouble());
    drn->SetMTOW(ui->ln_MTOW->text().toDouble());
    drn->Setautonomie(ui->ln_autonomie->text().toDouble());
    drn->Setconsomation(ui->ln_conso->text().toDouble());
    drn->setCharge_utile_max();

    drn->saveDrone();



}
